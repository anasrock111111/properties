import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Col, Modal, Row} from "reactstrap";
import FeatherIcon from "feather-icons-react";

class ModalProperties extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (
            <Modal size={'lg'} isOpen={this.props.modal} toggle={this.props.toggle}>
                <div className={'close-icon-container'} onClick={this.props.toggle}>
                    X
                </div>
                <h1 className={'modal-title'}>Unit : 305 - 3 Bedroom - Green Community Townhouse - Motor City -
                    Green Community</h1>

                <div className={'modal-body'}>

                    <Row>
                        <Col className={'modal-fixed-header-item-container'}>
                            <div className={'modal-fixed-header-item modal-fixed-header-item-no-border'}>
                                <div className={'modal-fixed-header-item-label'}>DEWA A/C No.</div>
                                <div className={'modal-fixed-header-item-value'}>383-10689-3</div>
                            </div>
                        </Col>

                        <Col className={'modal-fixed-header-item-container'}>
                            <div className={'modal-fixed-header-item '}>
                                <div className={'modal-fixed-header-item-label'}>Emicool No.</div>
                                <div className={'modal-fixed-header-item-value'}>383-10689-3</div>
                            </div>
                        </Col>
                        <Col className={'modal-fixed-header-item-container'}>
                            <div className={'modal-fixed-header-item modal-fixed-header-item-no-border'}>
                                <div className={'modal-fixed-header-item-label'}>Makani Number</div>
                                <div className={'modal-fixed-header-item-value'}>383-10689-3</div>
                            </div>
                        </Col>
                    </Row>

                    <Row className={'modal-properties-container'}>

                        {this.props.properties.map((e, i) =>

                            i === 12 || i === 15 ? <React.Fragment>
                                <Col sm={12}>
                                    <hr/>
                                </Col>
                                <Col lg={4} md={6} sm={12}>
                                    <div className={'property-details-item'}>

                                        <div className={'property-details-item-section-1'}>
                                            <FeatherIcon size={18} icon={e.icon}
                                                         className={'property-details-item-section-1-icon'}/>
                                            {e.label}
                                        </div>
                                        <div className={'property-details-item-section-2'}>
                                            {e.value}
                                        </div>
                                    </div>
                                </Col>
                            </React.Fragment> : <Col lg={4} md={6} sm={12}>
                                <div className={'property-details-item'}>

                                    <div className={'property-details-item-section-1'}>
                                        <FeatherIcon size={18} icon={e.icon}
                                                     className={'property-details-item-section-1-icon'}/>
                                        {e.label}
                                    </div>
                                    <div className={'property-details-item-section-2'}>
                                        {e.value}
                                    </div>
                                </div>
                            </Col>
                        )}

                    </Row>

                    <Row>

                        <Col>

                            <h1 className={'header-section-title'}>Description</h1>
                            <hr/>
                            <p className={'section-2-p black'}>Spectacular amounts of Sea View,.High quality
                                spacious Apartment
                                More value for money.</p>
                            <p className={'section-2-p black'}>Close to a community center which has retail and
                                other
                                facilities.</p>
                            <p className={'section-2-p black'}>Gated, family-friendly,suburban community. </p>
                            <p className={'section-2-p black'}>Close to school</p>
                        </Col>
                    </Row>

                </div>
            </Modal>
        );
    }
}

export default withRouter(ModalProperties);
