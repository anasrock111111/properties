import React, {Component} from "react";
import {
    withRouter,
    Route,
    Switch,
    BrowserRouter as Router, Link
} from "react-router-dom";

import {Row, Col} from "reactstrap";

class TabBodyHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <Row className={'tabBody-Header-container'}>
                    <Col md={9} xs={12} className={'d-flex justify-content-start align-items-center'}>
                        <div className={'h-badge-info'}>sale</div>
                        <div className={'title-tabBody'}>2 Bedroom - Apartment - Marina Residences, Trunk
                            West- Palm Jumeriah
                        </div>
                    </Col>
                    <Col md={3} xs={12} className={'d-flex justify-content-end align-items-center'}>

                        <div className={'title-tabBody'}>AED 1,749,000</div>
                        <div className={'icon-edit-tabBody d-flex align-items-center'}><img
                            src={require('./../../../assets/icons/edit-solid.svg')}/></div>

                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default withRouter(TabBodyHeader);
