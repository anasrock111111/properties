import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import FeatherIcon from "feather-icons-react";
import {withRouter} from 'react-router-dom';
import HeaderLinks from "../../../Lists/HeaderLinks/index";
import GalleryImages from "../../../Lists/GalleryImages";

class Gallery extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (
            <React.Fragment>
                <img className={'gallery-selected-image'}
                     src={require('./../../../assets/images/Property_Profile.png')}/>
                <div className={'d-flex gallery-footer-container'}>

                    <div className={'gallery-images-container'}>

                        <GalleryImages galleryImages={this.props.galleryImages}/>
                    </div>
                    <div className={'icon-edit-tabBody d-flex align-items-center'}><img
                        src={require('./../../../assets/icons/edit-solid.svg')}/></div>
                </div>
            </React.Fragment>
        );
    }
}

export default withRouter(Gallery);
