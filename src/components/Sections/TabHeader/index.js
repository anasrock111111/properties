import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import FeatherIcon from "feather-icons-react";
import {withRouter} from 'react-router-dom';
import HeaderLinks from "../../../Lists/HeaderLinks/index";

class TabHeader extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (
            <Row className={'section-tab'}>
                <Col xs={'auto'} className={'header-section-1-container'}>
                    <FeatherIcon icon={'arrow-left'} className={'icon-rounded'}/>

                </Col>
                <Col xs={'auto'} className={'header-section-2-container'}>

                    <Row className={'header-section-2'}>
                        <Col xs={'auto header-section-2-item'}>
                            <p className={'header-section-2-item-p color-green  '}>Published <i><FeatherIcon
                                icon={'arrow-down'} size={16} className={'color-red'}/></i></p>
                        </Col>
                        <Col xs={'auto header-section-2-item'}>
                            <p className={'header-section-2-item-p color-blue'}><i
                                className={'color-red'}>#</i> Ref
                                No. </p>
                        </Col>
                        <Col xs={'auto header-section-2-item'}>
                            <p className={'header-section-2-item-p text-bold'}>W1956509</p>
                        </Col>
                    </Row>

                </Col>
                <Col xs={'auto'} className={'header-section-2-container'}>
                    <HeaderLinks headerLinks={this.props.headerLinks}/>
                </Col>

            </Row>
        );
    }
}

export default withRouter(TabHeader);
