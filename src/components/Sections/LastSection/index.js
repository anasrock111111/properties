import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import FeatherIcon from "feather-icons-react";
import {withRouter} from 'react-router-dom';
import HeaderLinks from "../../../Lists/HeaderLinks/index";
import GalleryImages from "../../../Lists/GalleryImages";

class LastSection extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (


            <Row>
                <Col xs={12} className={'PropertyDetails-section-3-item'}>
                    <div className={'property-details-item'}>

                        <div className={'property-details-item-section-1'}>
                            Permit No.
                        </div>
                        <div className={'property-details-item-section-2'}>
                            5435436
                        </div>
                    </div>
                </Col>
                <Col xs={12} className={'PropertyDetails-section-3-item'}>
                    <div className={'property-details-item'}>

                        <div className={'property-details-item-section-1'}>
                            Listing Expiry
                        </div>
                        <div className={'property-details-item-section-2'}>
                            9 jun 2021
                        </div>
                    </div>
                </Col>

                <Col xs={12}>
                    <hr/>
                </Col>

                <Col xs={12} className={'PropertyDetails-section-3-item'}>
                    <div className={'property-details-item'}>

                        <div className={'property-details-item-section-1'}>
                            Listing Expiry
                        </div>
                        <div className={'property-details-item-section-2'}>
                            9 jun 2021
                        </div>
                    </div>
                </Col>


                <Col xs={12} className={'PropertyDetails-section-3-item'}>
                    <div className={'property-details-item'}>

                        <div className={'property-details-item-section-1'}>
                            Listing Expiry
                        </div>
                        <div className={'property-details-item-section-2'}>
                            9 jun 2021
                        </div>
                    </div>
                </Col>

                <Col xs={12}>
                    <hr/>
                </Col>

                <Col xs={12} className={'PropertyDetails-section-3-item'}>
                    <div className={'property-details-item'}>

                        <div className={'property-details-item-section-1'}>
                            Listing Expiry
                        </div>
                        <div className={'property-details-item-section-2'}>
                            9 jun 2021
                        </div>
                    </div>
                </Col>

                <Col xs={12} className={'PropertyDetails-section-3-item'}>
                    <div className={'property-details-item'}>

                        <div className={'property-details-item-section-1'}>
                            Listing Expiry
                        </div>
                        <div className={'property-details-item-section-2'}>
                            9 jun 2021
                        </div>
                    </div>
                </Col>

            </Row>

        );
    }
}

export default withRouter(LastSection);
