import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import FeatherIcon from "feather-icons-react";
import {Route, Switch, withRouter} from 'react-router-dom';
import HeaderLinks from "../../../Lists/HeaderLinks/index";
import PropertyDetails from "../../../pages/PropertyDetails";
import Offers from "../../../pages/Offers";

class SwitchTabsBody extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (
            <Switch>
                <Route path={'/'} exact component={PropertyDetails} key={'PropertyDetails'}/>
                <Route path={'/offers'} component={Offers} key={'Offers'}/>
            </Switch>
        );
    }
}

export default withRouter(SwitchTabsBody);
