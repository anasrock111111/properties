import React, {Component} from 'react';

import {Switch, Route, Link, useLocation} from "react-router-dom";
import {Col, Row} from "reactstrap";
import FeatherIcon from "feather-icons-react";
import {withRouter} from 'react-router-dom';

class PropertiesDefaultList extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (

            this.props.properties.map((e, i) =>

                <Col md={6} sm={12}>
                    <div className={'property-details-item'}>

                        <div className={'property-details-item-section-1'}>
                            <FeatherIcon size={18} icon={e.icon}
                                         className={'property-details-item-section-1-icon'}/>
                            {e.label}
                        </div>
                        <div className={'property-details-item-section-2'}>
                            {e.value}
                        </div>
                    </div>
                </Col>
            )
        );
    }
}

export default withRouter(PropertiesDefaultList);
