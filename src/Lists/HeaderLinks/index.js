import React, {Component} from 'react';

import {Switch, Route, Link, useLocation} from "react-router-dom";
import {Col, Row} from "reactstrap";
import FeatherIcon from "feather-icons-react";
import {withRouter} from 'react-router-dom';

class HeaderLinks extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (

            <ul className={'navbar-menu'}>
                {this.props.headerLinks.map((e, i) =>

                    <li className={this.props.location.pathname === e.link ? 'active' : ''}><Link to={e.link}>
                        <p>{e.hasBadge ? <div><i>{e.count}</i></div> : <React.Fragment/>}
                            {e.label}</p></Link></li>
                )}
            </ul>
        );
    }
}

export default withRouter(HeaderLinks);
