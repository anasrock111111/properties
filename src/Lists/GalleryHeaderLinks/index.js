import React, {Component} from 'react';

import {Switch, Route, Link, useLocation} from "react-router-dom";
import {Col, Row} from "reactstrap";
import FeatherIcon from "feather-icons-react";
import {withRouter} from 'react-router-dom';

class GalleryHeaderLinks extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (

            this.props.links.map((e, i) =>

                <div className={i === 0 ? 'gallery-tab-link gallery-active-tab' : 'gallery-tab-link'}>
                    <Link to={e.link}>
                        <div className={'text-center d-flex gallery-tab-item'}>
                            <img className={'gallery-tab-icon'}
                                 src={e.src}/>{e.label}
                        </div>
                    </Link>
                </div>
            )

        );
    }
}

export default withRouter(GalleryHeaderLinks);
