import React, {Component} from 'react';

import {Switch, Route, Link, useLocation} from "react-router-dom";
import {Col, Row} from "reactstrap";
import FeatherIcon from "feather-icons-react";
import {withRouter} from 'react-router-dom';

class GalleryImages extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (

            this.props.galleryImages.map((e, i) =>
                <img className={'gallery-images-item'}
                     src={e.src}/>
            )

        )
            ;
    }
}

export default withRouter(GalleryImages);
