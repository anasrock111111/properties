import React, {Component} from 'react';

import {Switch, Route, Link,} from "react-router-dom";
import {Container, Row, Button, Col, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import FeatherIcon from "feather-icons-react";
import GalleryImages from "../../Lists/GalleryImages";
import Gallery from "../../components/Sections/Gallery";
import GalleryHeaderLinks from "../../Lists/GalleryHeaderLinks";
import PropertiesDefaultList from "../../Lists/PropertiesDefaultList";
import LastSection from "../../components/Sections/LastSection";
import ModalProperties from "../../components/ModalProperties";

class PropertyDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {
            properties: [
                {
                    icon: 'airplay',
                    label: 'Type',
                    value: 'Apartment'
                }, {
                    icon: 'airplay',
                    label: 'Height',
                    value: '47'
                }, {
                    icon: 'airplay',
                    label: 'Bedrooms',
                    value: '2'
                }, {
                    icon: 'airplay',
                    label: 'Bathrooms',
                    value: '3'
                }, {
                    icon: 'airplay',
                    label: 'Built-Up',
                    value: '1800.00'
                }, {
                    icon: 'airplay',
                    label: 'Plot Size',
                    value: '0'
                }, {
                    icon: 'airplay',
                    label: 'Furnished',
                    value: 'furnished'
                }, {
                    icon: 'airplay',
                    label: 'Parking',
                    value: '2'
                }, {
                    icon: 'airplay',
                    label: 'Cooling',
                    value: 'Empower'
                }, {
                    icon: 'airplay',
                    label: 'Service Charge',
                    value: 'AED 15 per.Sqft'
                },
            ],
            propertiesAll: [
                {
                    icon: 'airplay',
                    label: 'Type',
                    value: 'Apartment'
                }, {
                    icon: 'airplay',
                    label: 'Height',
                    value: '47'
                }, {
                    icon: 'airplay',
                    label: 'Bedrooms',
                    value: '2'
                }, {
                    icon: 'airplay',
                    label: 'Bathrooms',
                    value: '3'
                }, {
                    icon: 'airplay',
                    label: 'Built-Up',
                    value: '1800.00'
                }, {
                    icon: 'airplay',
                    label: 'Plot Size',
                    value: '0'
                }, {
                    icon: 'airplay',
                    label: 'Furnished',
                    value: 'furnished'
                }, {
                    icon: 'airplay',
                    label: 'Balcony',
                    value: 'Yes'
                }, {
                    icon: 'airplay',
                    label: 'Parking',
                    value: '2'
                }, {
                    icon: 'airplay',
                    label: 'View',
                    value: 'Community'
                }, {
                    icon: 'airplay',
                    label: 'Cooling',
                    value: 'Empower'
                }, {
                    icon: 'airplay',
                    label: 'Service Charge',
                    value: 'AED 15 per.Sqft'
                }, {
                    icon: 'airplay',
                    label: 'Maid Room',
                    value: 'No'
                }, {
                    icon: 'airplay',
                    label: 'Storage Room',
                    value: 'No'
                }, {
                    icon: 'airplay',
                    label: 'Laundry Room',
                    value: 'No'
                }, {
                    icon: 'airplay',
                    label: 'Pool',
                    value: 'Shared Pool'
                }, {
                    icon: 'airplay',
                    label: 'Gym',
                    value: 'Shared Gym'
                }, {
                    icon: 'airplay',
                    label: 'Security',
                    value: 'Yes'
                }, {
                    icon: 'airplay',
                    label: 'Garden',
                    value: 'Yes'
                },
            ],
            galleryImages: [
                {
                    src: require('./../../assets/images/room.png'),
                }, {
                    src: require('./../../assets/images/room.png'),
                }, {
                    src: require('./../../assets/images/room.png'),
                }
            ],
            galleryLinks: [

                {
                    label: 'Photos',
                    src: require('./../../assets/icons/photo.svg'),
                    link: '/',

                }, {
                    label: 'Floor Plan',
                    src: require('./../../assets/icons/floor.svg'),
                    link: '/',

                }, {
                    label: '360 View',
                    src: require('./../../assets/icons/360-degree.svg'),
                    link: '/',

                }
            ],
            modal: false
        }
    }

    toggle = () => this.setState({modal: !this.state.modal});

    render() {
        return (
            <React.Fragment>

                <Row className={'PropertyDetails-container'}>
                    <Col lg={4} md={6} sm={12}>
                        <div className={'gallery-tab-header'}>

                            <GalleryHeaderLinks links={this.state.galleryLinks}/>
                        </div>

                        <Gallery galleryImages={this.state.galleryImages}/>

                    </Col>
                    <Col lg={5} md={6} sm={12}>

                        <h1 className={'header-section-title'}>Property Details</h1>
                        <hr/>

                        <Row>
                            <PropertiesDefaultList properties={this.state.properties}/>
                        </Row>

                        <div className={'footer-section-2-container'} onClick={this.toggle}>

                            <div className={'footer-section-2-text'}>more details</div>
                            <img className={'gallery-tab-icon'}
                                 src={require('./../../assets/icons/zoom.svg')}/>
                        </div>


                        <h1 className={'header-section-title'}>Description</h1>
                        <hr/>
                        <p className={'section-2-p'}>Spectacular amounts of Sea View,.High quality spacious Apartment
                            More value for money.</p>
                        <p className={'section-2-p'}>Close to a community center which has retail and other
                            facilities.</p>
                        <p className={'section-2-p'}>Gated, family-friendly,suburban community. </p>
                        <p className={'section-2-p'}>Close to school</p>

                    </Col>
                    <Col lg={3} md={6} sm={12}>

                        <div className={'PropertyDetails-section-3-container'}>
                            <LastSection/>
                        </div>
                    </Col>
                </Row>

                <ModalProperties properties={this.state.propertiesAll} modal={this.state.modal} toggle={this.toggle}/>


            </React.Fragment>
        );
    }
}

export default PropertyDetails;
