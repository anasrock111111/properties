import React, {Component} from "react";
import {
    withRouter,
    Route,
    Switch,
    BrowserRouter as Router, Link
} from "react-router-dom";

// Import Scss
import "./theme.scss";

import Offers from "./pages/Offers/";
import TabHeader from "./components/Sections/TabHeader";
import TabBodyHeader from "./components/Sections/TabBodyHeader";
import SwitchTabsBody from "./components/Sections/SwitchTabsBody";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            headerLinks: [
                {
                    label: 'Property Details',
                    link: '/',
                    hasBadge: 0
                },
                {
                    label: 'Offers',
                    link: '/offers',
                    hasBadge: true,
                    count: 0
                },
                {
                    label: 'Listing statistic',
                    link: '/s',
                    hasBadge: false,
                    count: 0
                },
                {
                    label: 'Market report',
                    link: '/m',
                    hasBadge: false,
                    count: 0
                },
                {
                    label: 'Document',
                    link: '/n',
                    hasBadge: false,
                    count: 0
                },
                {
                    label: 'Calender',
                    link: '/r',
                    hasBadge: false,
                    count: 0
                },
                {
                    label: 'History',
                    link: '/f',
                    hasBadge: false,
                    count: 0
                }
            ]
        }
    }


    render() {
        return (
            <React.Fragment>
                <div className="container-section text-center">

                    <div className="section-tab-container ">

                        <TabHeader headerLinks={this.state.headerLinks}/>

                        <div className={'tabBody'}>

                            <TabBodyHeader/>

                            <SwitchTabsBody/>

                        </div>
                    </div>
                </div>

            </React.Fragment>
        );
    }
}

export default withRouter(App);
